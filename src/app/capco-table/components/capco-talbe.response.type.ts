export interface IgetDataRESPONSEType {
    data: any[];
    showRowOptions: number[];
    paginationButtons: number[];
}
