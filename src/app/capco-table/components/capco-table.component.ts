import { Component, OnInit } from '@angular/core';
import { CapcoTableService } from '../service/capco-table.service';
import { IuserProfile } from './capco-table.data.type';
import { IgetDataRESPONSEType } from './capco-talbe.response.type';

@Component({
  selector: 'app-capco-table',
  templateUrl: './capco-table.component.html',
  styleUrls: ['./capco-table.component.css'],
})
export class CapcoTableComponent implements OnInit {
  // showRows dropdown
  showRowsCount = 50;
  showRowsOptions: number[] = [50];
  // Pagination Buttons
  activePaginationButton = 1;
  paginationButtons: any = [];
  // table data
  userProfiles: IuserProfile[];

  constructor(private capcoTableService: CapcoTableService) { }

  ngOnInit() {
    this.loadData();
  }
  /*load data will fetch sample data*/
  loadData() {
    this.capcoTableService.getData((this.showRowsCount * 1), (this.activePaginationButton * 1))
      .subscribe((response: IgetDataRESPONSEType) => {
        this.userProfiles = response.data;
        this.showRowsOptions = response.showRowOptions;
        this.paginationButtons = response.paginationButtons;
      });
  }
  /*On click on pagination button this method will be called */
  changePage(acitvePageNum: string) {
    this.activePaginationButton = parseInt(acitvePageNum, 10);
    this.loadData();
  }
  /*On click on select dropdown this method will be called */
  changeShowRows() {
    this.loadData();
  }

  postData(id: string, status: string) {
    this.capcoTableService.postData(id, status).subscribe((data) => {
      alert(data);
    });
  }

}
