import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CapcoTableComponent } from './capco-table.component';

describe('CounterComponent ', () => {
    let component: CapcoTableComponent;
    let fixture: ComponentFixture<CapcoTableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CapcoTableComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CapcoTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('can load instance', () => {
        expect(component).toBeDefined();
    });

    it('should run loadData', () => {
        component.showRowsCount = 50;
        component.activePaginationButton = 1;
        component.userProfiles = [];
        component.showRowsOptions = [];
        component.paginationButtons = [];

        component.loadData();

        expect(component.userProfiles.length).toEqual(50);
        expect(component.paginationButtons.length).toEqual(4);
    });

    it('should run load on change changeShowRows', () => {
        component.showRowsCount = 100;
        component.activePaginationButton = 1;
        component.userProfiles = [];
        component.showRowsOptions = [];
        component.paginationButtons = [];

        component.changeShowRows();

        expect(component.userProfiles.length).toEqual(100);
        expect(component.paginationButtons.length).toEqual(2);
    });

});
