import { TestBed } from '@angular/core/testing';
import { CapcoTableService } from './capco-table.service';
import { IgetDataRESPONSEType } from '../components/capco-talbe.response.type';

describe('CapcoTableService', () => {
  let service: CapcoTableService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [CapcoTableService]
    });
    service = TestBed.get(CapcoTableService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  it('should run #getData()',
    (done: DoneFn) => {
      service.getData(50, 1).subscribe((value: IgetDataRESPONSEType) => {
        expect(value.data.length).toBe(50);
        expect(value.showRowOptions.length).toBe(4);
        expect(value.paginationButtons.length).toBe(4);
        done();
      });
    });

  it('should run getRequestDataPortion()',
    (done: DoneFn) => {
      spyOn((service as any), 'getRequestDataPortion').and.callThrough();
      service.getData(50, 1).subscribe((value: IgetDataRESPONSEType) => {
        expect((service as any).getRequestDataPortion).toHaveBeenCalled();
        done();
      });
    });

  it('should run _setShowRowOptions',
    (done: DoneFn) => {
      spyOn((service as any), '_setShowRowOptions').and.callThrough();
      service.getData(50, 1).subscribe((value: IgetDataRESPONSEType) => {
        expect((service as any)._setShowRowOptions).toHaveBeenCalled();
        done();
      });
    });

  it('should run _setpaginationButtons',
    (done: DoneFn) => {
      spyOn((service as any), '_setpaginationButtons').and.callThrough();
      service.getData(50, 1).subscribe((value: IgetDataRESPONSEType) => {
        expect((service as any)._setpaginationButtons).toHaveBeenCalled();
        done();
      });
    });

  it('should run #postData()',
    (done: DoneFn) => {
      service.postData('1', 'read').subscribe((value: string) => {
        expect(value).toBe(`id: 1, status: read, sent success`);
        done();
      });
    });

});
