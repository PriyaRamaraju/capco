import { Injectable } from '@angular/core';
import { of, from } from 'rxjs';
import * as sampleData from '../../../data/sample_data.json';
import { IgetDataRESPONSEType } from '../components/capco-talbe.response.type';

@Injectable({
  providedIn: 'root'
})
export class CapcoTableService {
  private defalutData: any = [];
  private defaultShowRows = 50;
  private responseData: IgetDataRESPONSEType = {
    data: [],
    showRowOptions: [],
    paginationButtons: []
  };

  constructor() { }
  /* getData Data method featch json and return the data alogn with pagination
   buttons cound and show row optinos values */
  getData(showRows: number, activePageNumber: number) {
    this.defalutData = sampleData.default;
    const dataPortion = this.getRequestDataPortion(showRows, activePageNumber);
    const dataParts = Math.ceil(this.defalutData.length / showRows);

    this._setpaginationButtons(dataParts);

    if (this.responseData.showRowOptions.length === 0) {
      this._setShowRowOptions(dataParts);
    }

    return of({ ...this.responseData, data: dataPortion });
  }
  /* Post Data method will post the data */
  postData(id: string, status: string) {
    return of(`id: ${id}, status: ${status}, sent success`);
  }
  /* _setShowRowOptions method will calculate show row options count */
  private _setShowRowOptions(dataParts) {
    for (let i = 1; i <= dataParts; i++) {
      this.responseData.showRowOptions.push(i * this.defaultShowRows);
    }
  }
  /* _setpaginationButtons method will calculate pagination buttons count*/
  private _setpaginationButtons(dataParts) {
    this.responseData.paginationButtons = [];

    for (let i = 1; i <= dataParts; i++) {
      this.responseData.paginationButtons.push(i);
    }
  }
  /* getRequestDataPortion method will get data of selected rage*/
  private getRequestDataPortion(showRowsCount: number, activePaginationButtonNum: number) {
    const startIndex = (activePaginationButtonNum * showRowsCount) - (showRowsCount);
    const endIndex = (activePaginationButtonNum * showRowsCount);

    return this.defalutData.slice(startIndex, endIndex);
  }
}
